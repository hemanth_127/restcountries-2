import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Routes, Route } from 'react-router-dom'
import './App.css'

import Heading from './components/Heading'
import Searcher from './components/Searcher'
import Countries from './components/Countries'
import RegionSelector from './components/RegionSelector'
import SubRegionSelector from './components/SubRegionSelector'
import PopulationSorter from './components/PopulationSorter'
import AreaSort from './components/AreaSort'
import Details from './components/Details'
import { Currency } from './components/Currency'

function App () {
  const [countries, setCountries] = useState([])
  const [searchTerm, setSearchTerm] = useState('')
  const [selectedRegion, setSelectedRegion] = useState('')
  const [selectedSubRegion, setSelectedSubRegion] = useState('')
  const [sortByPopulation, setSortByPopulation] = useState('')
  const [sortByArea, setSortByArea] = useState('')
  const [currency, setCurrency] = useState('')

  useEffect(() => {
    const fetchCountries = async () => {
      try {
        const response = await fetch('https://restcountries.com/v3.1/all')
        if (!response.ok) {
          throw new Error("We are sorry !,Couldn't fetch resource")
        }
        const data = await response.json()
        setCountries(data)
      } catch (error) {
        console.error(error.message)
      }
    }
    fetchCountries()
  }, [])

  const getRegions = () => {
    let regions = []
    countries.map(country => {
      if (country.region && !regions.includes(country.region)) {
        regions.push(country.region)
      }
    })
    return regions.sort()
  }

  const getSubRegions = () => {
    let subRegion = []
    countries.map(country => {
      if (
        country.region === selectedRegion &&
        country.subregion &&
        !subRegion.includes(country.subregion)
      ) {
        subRegion.push(country.subregion)
      }
    })
    return subRegion.sort()
  }

  const getCurrency = () => {
    let currency = []
    countries.forEach(country => {
      if (
        (!selectedRegion && !selectedSubRegion) ||
        (!selectedSubRegion && country.region === selectedRegion) ||
        (selectedSubRegion && country.subregion === selectedSubRegion)
      ) {
        if (country.currencies) {
          Object.values(country.currencies).forEach(currencyObj => {
            currency.push(currencyObj.name)
          })
        }
      }
    })
    const currencyList = Array.from(new Set(currency))
    return currencyList
  }

  const handleRegionChange = selectedRegion => {
    setSelectedRegion(selectedRegion)
    setSelectedSubRegion('')
    setCurrency('')
  }

  const handleSubRegionChange = selectedSubRegion => {
    setSelectedSubRegion(selectedSubRegion)
    setCurrency('')
  }

  const handleSortChange = sortByPopulation => {
    setSortByArea('')
    setSortByPopulation(sortByPopulation)
  }

  const handleCurrencyChange = currency => {
    setCurrency(currency)
  }

  
  return (
    <>
      <div className='container'>
        <Heading />
        <Routes>
          <Route
            path='/'
            element={
            <div>
                <section className='searcher'>
                    <Searcher setSearchTerm={setSearchTerm} />
                    <Currency currencyData={getCurrency()} setCurrency={handleCurrencyChange} />
                    <RegionSelector setSelectedRegion={handleRegionChange} regions={getRegions()} />
                    <PopulationSorter setSortByPopulation={handleSortChange} />
                    <AreaSort setSortByArea={setSortByArea} />

                    {selectedRegion && (
                        <SubRegionSelector  setSelectedSubRegion={handleSubRegionChange} subRegions={getSubRegions()} />
                    )}

                </section>

                <section className='grid-section'>
                    <Countries
                        countries={countries}
                        searchTerm={searchTerm}
                        filterRegion={selectedRegion}
                        filterSubRegion={selectedSubRegion}
                        sortByPopulation={sortByPopulation}
                        sortByArea={sortByArea}
                        currency={currency}
                    />
                </section>
                </div>
            }
           />
          <Route path='/country/:id' element={<Details />} />
        </Routes>
        <Details />
      </div>
    </>
  )
}

export default App
