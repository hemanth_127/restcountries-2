import React from 'react'

const RegionSelector = ({ regions, setSelectedRegion }) => {
 
  return (
    <select onChange={(e)=>{setSelectedRegion(e.target.value)}}>

      <option value=''>Select Regions </option>
      {regions.map((region, index) => (
        <option key={index} value={region}>
          {region}
        </option>
      ))}
    </select>
  )
}

export default RegionSelector
