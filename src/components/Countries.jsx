import React from 'react'
import { Link, useNavigate } from 'react-router-dom'

const Countries = ({
  countries,
  searchTerm,
  filterRegion,
  filterSubRegion,
  sortByPopulation,
  sortByArea,
  currency
}) => {



  const navigate = useNavigate()
  const filterCountry = country => {
    const {
        name: { common: name },
        region,
        subregion,
        currencies
    } = country

    const searchMatch = name.toLowerCase().includes(searchTerm.toLowerCase())
    const regionMatch = filterRegion ? region === filterRegion : true
    const subRegionMatch = filterSubRegion? subregion === filterSubRegion: true

    const currencyMatch = currency && currencies &&
      Object.values(currencies).some(
        currencyObj => currencyObj.name === currency
      )

    return currency
      ? searchMatch && regionMatch && subRegionMatch && currencyMatch
      : searchMatch && regionMatch && subRegionMatch
  }

  function handleDetailView (ccn3) {
    navigate(`country/${ccn3}`)
  }

  let filteredCountries = countries.filter(filterCountry)

  if (sortByPopulation) {
    filteredCountries = filteredCountries.slice().sort((a, b) => {
      return sortByPopulation === 'Asc'
        ? a.population - b.population
        : b.population - a.population
    })
  }
  if (sortByArea) {
    filteredCountries = filteredCountries.slice().sort((a, b) => {
      return sortByArea === 'Asc1' ? a.area - b.area : b.area - a.area
    })
  }

  return filteredCountries.map((country, index) => {
    const {
      name: { common: name },
      population,
      region,
      capital,
      ccn3,
      flags: { png: flags }
    } = country

    return (
      <article
        key={index}
        className='article'
        onClick={() => handleDetailView(ccn3)}
      >
        <Link to={`country/${ccn3}`}>
          <img
            className='image'
            src={flags}
            alt={name}
            style={{ width: '100%', height: 200 }}
          />
        </Link>

        <div className='article-div'>
          <h3>{name}</h3>
          <p>
            Population: <span>{population}</span>
          </p>
          <p>
            Region: <span>{region}</span>{' '}
          </p>
          <p>
            {' '}
            Capital: <span>{capital || 'unavailabale'}</span>{' '}
          </p>
        </div>
      </article>
    )
  })
}

export default Countries
