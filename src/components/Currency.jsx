import React from 'react'

export const Currency = ({ setCurrency, currencyData }) => {
  return (
    <select
    style={{width:'10vw'}}
      onChange={e => {

        setCurrency(e.target.value)
      }}
    >
      <option value=''>Select Currency </option>
      {currencyData.map((cur, index) => (
        <option key={index} value={cur}>
          {cur}
        </option>
      ))}
    </select>
  )
}
