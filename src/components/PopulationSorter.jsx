import React from 'react'

const PopulationSorter = ({ setSortByPopulation }) => {
 
  return (
    <div>
      Sort Population
        <select
            name='population'
            id='hel'
            onChange={e => {
                setSortByPopulation(e.target.value)
            }}
        >

        <option value=''>None</option>
        <option value='Asc'>Ascending</option>
        <option value='Desc'>Descending</option>
      </select>
    </div>
  )
}

export default PopulationSorter
