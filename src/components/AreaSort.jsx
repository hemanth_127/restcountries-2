import React from 'react'

const AreaSort = ({ setSortByArea }) => {


  return (
    <div>
      Sort Area
      <select name='area' id='hel' onChange={(e)=>{setSortByArea(e.target.value)}}>
        <option value=''>None</option>
        <option value='Asc1'>Ascending</option>
        <option value='Desc1'>Descending</option>
      </select>
    </div>
  )
}

export default AreaSort
