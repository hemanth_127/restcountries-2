import React from 'react'

const SubRegionSelector = ({ subRegions, setSelectedSubRegion }) => {
  return (
    <select onChange={e => setSelectedSubRegion(e.target.value)}>
      <option value=''>Select SubRegions</option>
      {subRegions.map((subregion, index) => (
        <option key={index} value={subregion}>
          {subregion}
        </option>
      ))}
    </select>
  )
}

export default SubRegionSelector
