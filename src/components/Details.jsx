import React, { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'

export default function Details () {
  const navigate = useNavigate()
  const [details, setDetails] = useState([])
  const { id } = useParams()
  if (!id) return <> loading ...</>;

  const fetchCountry = async () => {
    try {
      const response = await fetch(`https://restcountries.com/v3.1/alpha/${id}`)
      if (!response.ok) {
        throw new Error("Couldn't fetch country data")
      }
      const data = await response.json()
      setDetails(data)
    } catch (error) {
      console.log(error.message)
    }
  }

  useEffect(() => {
    fetchCountry()
  }, [id])

  if (details.length <= 0) {
    return <p>Loading...</p>
  }

  const {
    name,
    population,
    region,
    capital,
    subregion,
    currencies,
    languages,
    flags: { png: flags },
    borders
  } = details[0]

  return (
    <>
      <div className='row'>
        <div
          className='detail-btn'
          onClick={() => {
            navigate(-1)
          }}
        >
          <button className='navbar-btn'>{'<- Back'}</button>
        </div>

        <div className='detail'>
          <div>
              <img
                className='image'
                src={flags}
                alt='flag'
                style={{
                  width: '30vw',
                  height: 300
                }}
              />
          </div>

          <article className='detail-articles'>
            <div className='detail-article'>
              <div className='detail-article-div'>
                <h3>{name.common}</h3>

                <p>
                  nativeName: &nbsp;
                  {name?.nativeName?.tur?.common ||
                    name?.nativeName?.eng?.common ||
                    'not available'}
                </p>

                <p>
                  Population: <span>{population}</span>
                </p>
                <p>
                  Region: <span>{region}</span>
                </p>
                <p>subRegion: {subregion}</p>
                <p>
                  Capital: <span>{capital}</span>
                </p>
              </div>
              <div className='detail-article-div art'>
                <p>
                  Currencies: &nbsp;
                  {currencies ? (
                    Object.keys(currencies).map(currency => (
                      <span key={currency}>
                        {currencies[currency]?.name} ({currency})
                      </span>
                    ))
                  ) : (
                    <span>Data not available</span>
                  )}
                </p>
              </div>
            </div>
            <div className='detail-article2'>
              <p>
                Languages: &nbsp;
                {languages ? (
                  Object.values(languages).map(language => (
                    <span key={language}>{language}</span>
                  ))
                ) : (
                  <span>Data not available</span>
                )}
              </p>
            </div>
          </article>
        </div>
      </div>
    </>
  )
}
