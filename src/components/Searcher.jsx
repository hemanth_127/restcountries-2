import React from 'react'

const Searcher = ({setSearchTerm}) => {
  
  const handleSearch = event => {
    setSearchTerm(event.target.value.toLowerCase())
  }
  return (
    <>
      <div>
        <input type='text' placeholder='search by country' onChange={handleSearch}  />
      </div>
    </>
  )
}

export default Searcher
